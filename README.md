# Timetable

Sample project which uses the [SBB](http://sbb.ch) timetable and also shows the data from the "Jahresformation" if available. This allows to see which type of train is arriving.

![Screenshot of application](screenshot.png)

## Docker

This image can be found on docker hub: [hub.docker.com/r/rootlogin/timetable](https://hub.docker.com/r/rootlogin/timetable)

Run with: `docker run -p 8080:8080 rootlogin/timetable` and open [localhost:8080](http://localhost:8080).

## Developer documentation

### Dependencies

* Java 1.8 JDK
* Maven 3.6

### Consumed APIs

* [OpenData Transport API](http://transport.opendata.ch/)
* [SBB "Jahresformation" API](https://data.sbb.ch/explore/dataset/jahresformation/information/)

### Used technologies

* [Spring Boot](https://spring.io/projects/spring-boot)
* [Apache HTTPComponents](https://hc.apache.org/)
* [JSON-B](http://json-b.net/)

### Build & Run Application

First you need to build the application with maven:

```shell script
mvn package

java -jar target/timetable-0.0.1-SNAPSHOT.jar
```

Afterwards open your browser under [localhost:8080](http://localhost:8080) and profit!

