FROM maven:3-jdk-13 as builder

RUN mkdir /tmp/build

COPY src/ /tmp/build/src/
COPY pom.xml /tmp/build

WORKDIR /tmp/build

RUN mvn package

FROM openjdk:13-alpine

COPY --from=builder /tmp/build/target/timetable-*.jar /opt/timetable.jar

EXPOSE 8080

# Modify docker entrypoint
CMD ["java", "-jar", "/opt/timetable.jar"]
