package ch.rootlogin.timetable.pojo;

public class ConnectionPojo {
    private JourneyStepPojo from;
    private JourneyStepPojo to;
    private SectionPojo[] sections;

    public JourneyStepPojo getFrom() {
        return from;
    }

    public void setFrom(JourneyStepPojo from) {
        this.from = from;
    }

    public JourneyStepPojo getTo() {
        return to;
    }

    public void setTo(JourneyStepPojo to) {
        this.to = to;
    }

    public SectionPojo[] getSections() {
        return sections;
    }

    public void setSections(SectionPojo[] sections) {
        this.sections = sections;
    }
}


