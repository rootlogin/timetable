package ch.rootlogin.timetable.pojo;

public class SectionPojo {
    private JourneyPojo journey;
    private JourneyStepPojo departure;
    private JourneyStepPojo arrival;

    public JourneyPojo getJourney() {
        return journey;
    }

    public void setJourney(JourneyPojo journeyPojo) {
        this.journey = journeyPojo;
    }

    public JourneyStepPojo getDeparture() {
        return departure;
    }

    public void setDeparture(JourneyStepPojo departure) {
        this.departure = departure;
    }

    public JourneyStepPojo getArrival() {
        return arrival;
    }

    public void setArrival(JourneyStepPojo arrival) {
        this.arrival = arrival;
    }
}
