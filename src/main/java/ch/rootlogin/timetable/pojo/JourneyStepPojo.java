package ch.rootlogin.timetable.pojo;

import javax.json.bind.annotation.JsonbProperty;

public class JourneyStepPojo {
    private StationPojo station;

    @JsonbProperty(nillable = true)
    private Integer arrivalTimestamp;

    @JsonbProperty(nillable = true)
    private Integer departureTimestamp;

    @JsonbProperty(nillable = true)
    private String platform;

    public StationPojo getStation() {
        return station;
    }

    public void setStation(StationPojo station) {
        this.station = station;
    }

    /*public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getArrivalTimestamp() {
        return arrivalTimestamp;
    }

    public void setArrivalTimestamp(String arrivalTimestamp) {
        this.arrivalTimestamp = arrivalTimestamp;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public int getDepartureTimestamp() {
        return departureTimestamp;
    }

    public void setDepartureTimestamp(int departureTimestamp) {
        this.departureTimestamp = departureTimestamp;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Prognosis getPrognosis() {
        return prognosis;
    }

    public void setPrognosis(Prognosis prognosis) {
        this.prognosis = prognosis;
    }

    public String getRealtimeAvailability() {
        return realtimeAvailability;
    }

    public void setRealtimeAvailability(String realtimeAvailability) {
        this.realtimeAvailability = realtimeAvailability;
    }

    public Station getLocation() {
        return location;
    }

    public void setLocation(Station location) {
        this.location = location;
    }*/

    public Integer getArrivalTimestamp() {
        return arrivalTimestamp;
    }

    public void setArrivalTimestamp(Integer arrivalTimestamp) {
        this.arrivalTimestamp = arrivalTimestamp;
    }

    public Integer getDepartureTimestamp() {
        return departureTimestamp;
    }

    public void setDepartureTimestamp(Integer departureTimestamp) {
        this.departureTimestamp = departureTimestamp;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}
