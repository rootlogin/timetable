package ch.rootlogin.timetable.pojo;

public class RecordPojo <T> {
    private T record;

    public T getRecord() {
        return record;
    }

    public void setRecord(T record) {
        this.record = record;
    }
}
