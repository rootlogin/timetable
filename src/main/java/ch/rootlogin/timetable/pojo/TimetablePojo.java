package ch.rootlogin.timetable.pojo;

public class TimetablePojo {
    private ConnectionPojo[] connections;
    private StationPojo from;
    private StationPojo to;

    public ConnectionPojo[] getConnections() {
        return connections;
    }

    public void setConnections(ConnectionPojo[] connectionPojos) {
        this.connections = connectionPojos;
    }

    public StationPojo getFrom() {
        return from;
    }

    public void setFrom(StationPojo from) {
        this.from = from;
    }

    public StationPojo getTo() {
        return to;
    }

    public void setTo(StationPojo to) {
        this.to = to;
    }
}
