package ch.rootlogin.timetable.pojo;

import java.util.HashMap;

public class JahresformationRecordPojo {
    private HashMap<String, Object> fields;

    public HashMap<String, Object> getFields() {
        return fields;
    }

    public void setFields(HashMap<String, Object> fields) {
        this.fields = fields;
    }
}
