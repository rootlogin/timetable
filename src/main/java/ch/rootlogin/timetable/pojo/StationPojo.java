package ch.rootlogin.timetable.pojo;

public class StationPojo {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
