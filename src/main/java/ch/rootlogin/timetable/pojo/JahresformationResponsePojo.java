package ch.rootlogin.timetable.pojo;

import javax.json.bind.annotation.JsonbProperty;
import java.util.List;

public class JahresformationResponsePojo {
    @JsonbProperty(nillable = true)
    private Integer totalCount;

    private List<RecordPojo<JahresformationRecordPojo>> records;

    //private RecordPojo<JahresformationRecordPojo> records;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public List<RecordPojo<JahresformationRecordPojo>> getRecords() {
        return records;
    }

    public void setRecords(List<RecordPojo<JahresformationRecordPojo>> records) {
        this.records = records;
    }
}
