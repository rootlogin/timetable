package ch.rootlogin.timetable.controller;

import ch.rootlogin.timetable.helper.Utils;
import ch.rootlogin.timetable.model.Connection;
import ch.rootlogin.timetable.model.JourneyStep;
import ch.rootlogin.timetable.model.Timetable;
import ch.rootlogin.timetable.pojo.*;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Controller
public class TimetableController {
    private final int CONNECTION_TIMEOUT_MS = 10 * 1000; // Timeout in millis.
    private final LocalDate START_TIMETABLE_PERIOD = LocalDate.of(2019,12,15);

    // connection timeout
    private final RequestConfig requestConfig = RequestConfig.custom()
            .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
            .setConnectTimeout(CONNECTION_TIMEOUT_MS)
            .setSocketTimeout(CONNECTION_TIMEOUT_MS)
            .build();

    private final CloseableHttpClient httpClient = HttpClients.createDefault();
    private final Jsonb jsonb = JsonbBuilder.create();

    private final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @GetMapping("/")
    public String timetableForm(Model model) {
        model.addAttribute("timetable", new Timetable("Grenchen", "Solothurn"));

        return "timetable";
    }

    @PostMapping("/")
    public String timetableSubmit(@ModelAttribute Timetable timetable) {
        try {
            String url = String.format(
                    "http://transport.opendata.ch/v1/connections?from=%s&to=%s&date=%s&time=%s",
                    URLEncoder.encode(timetable.getFrom(),"UTF-8"),
                    URLEncoder.encode(timetable.getTo(), "UTF-8"),
                    timetable.getWhen().format(dateFormatter),
                    timetable.getWhen().format(timeFormatter)
            );

            HttpGet request = new HttpGet(url);
            request.setHeader("Accept", "application/json");
            request.setConfig(requestConfig);

            try {
                CloseableHttpResponse response = httpClient.execute(request);

                if(response.getStatusLine().getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();

                    if (entity != null) {
                        timetable.setConnections(parseTimetableResponse(entity.getContent()));
                    }
                }

                // ensure response is closed.
                response.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "timetable";
    }

    private List<Connection> parseTimetableResponse(InputStream timetableResponse) {
        TimetablePojo timetablePojo = jsonb.fromJson(timetableResponse, TimetablePojo.class);

        ArrayList<Connection> connections = new ArrayList<>();
        for(ConnectionPojo conn : timetablePojo.getConnections()) {
            // parse date
            int departureTimestamp = conn.getFrom().getDepartureTimestamp();
            int arrivalTimestamp = conn.getTo().getArrivalTimestamp();

            Connection connection = new Connection(
                    Utils.getDateTimeFromTimestamp(departureTimestamp),
                    Utils.getDateTimeFromTimestamp(arrivalTimestamp),
                    conn.getFrom().getPlatform()
            );

            connection.setJourneySteps(new ArrayList<>());
            for(SectionPojo section : conn.getSections()) {
                if(section.getJourney() == null) {
                    continue;
                }

                // parse date
                int secDepartureTimestamp = section.getDeparture().getDepartureTimestamp();
                int secArrivalTimestamp = section.getArrival().getArrivalTimestamp();

                JourneyStep journeyStep = new JourneyStep(
                        section.getDeparture().getStation().getName(),
                        section.getArrival().getStation().getName(),
                        Utils.getDateTimeFromTimestamp(secDepartureTimestamp),
                        Utils.getDateTimeFromTimestamp(secArrivalTimestamp),
                        section.getDeparture().getPlatform()
                );

                if(section.getJourney().getOperator() != null) {
                    journeyStep.setOperator(section.getJourney().getOperator());
                }

                journeyStep.setTrainName(section.getJourney().getName());

                // data sanitation
                switch(section.getJourney().getCategory()) {
                    // ICE is a train type itself
                    case "ICE":
                        journeyStep.setProduct(section.getJourney().getNumber());
                        break;

                    // train category in number included
                    case "EC":
                    case "R":
                    case "RE":
                        journeyStep.setProduct(section.getJourney().getNumber());

                        // read train data from jahresformation
                        getTrainData(journeyStep, section.getJourney().getName());
                        break;

                    // Busses
                    case "B":
                        journeyStep.setProduct(String.format("Bus %s",section.getJourney().getNumber()));
                        break;

                    // Trams
                    case "T":
                        journeyStep.setProduct(String.format("Tram %s",section.getJourney().getNumber()));
                        break;

                    // all other trains, busses
                    default:
                        journeyStep.setProduct(String.format("%s %s", section.getJourney().getCategory(), section.getJourney().getNumber()));

                        // read train data from jahresformation
                        getTrainData(journeyStep, section.getJourney().getName());
                }

                connection.getJourneySteps().add(journeyStep);
            }

            connections.add(connection);
        }

        return connections;
    }

    private void getTrainData(JourneyStep journeyStep, String name) {
        // Limit Connect


        // real train number is in the train name
        String[] nameSplitted = name.split(" ");
        if(nameSplitted.length < 2) {
            return;
        }

        String timeZone = TimeZone.getDefault().getID();
        String query = String.format("zug=%s.0 AND beginnfahrplanperiode='FPL-2020'", nameSplitted[1]);

        try {
            HttpGet request = new HttpGet(String.format("https://data.sbb.ch/api/v2/catalog/datasets/jahresformation/records?where=%s&rows=10&pretty=false&timezone=%s", URLEncoder.encode(query, "UTF-8"), timeZone));
            request.setHeader("Accept", "application/json");
            request.setConfig(requestConfig);

            try {
                CloseableHttpResponse response = httpClient.execute(request);

                if(response.getStatusLine().getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();

                    if (entity != null) {
                        JahresformationResponsePojo jahresformationResponse = jsonb.fromJson(entity.getContent(), JahresformationResponsePojo.class);

                        if(jahresformationResponse.getRecords() != null) {
                            for(RecordPojo<JahresformationRecordPojo> record : jahresformationResponse.getRecords()) {
                                HashMap<String, Object> recordFields = record.getRecord().getFields();

                                if(
                                        !recordFields.containsKey("bitmap")
                                        || !recordFields.containsKey("block_bezeichnung")
                                ) {
                                    // close response connection
                                    response.close();
                                    return;
                                }

                                // calculate days
                                int daysSinceTimetableChange = Math.toIntExact(ChronoUnit.DAYS.between(START_TIMETABLE_PERIOD, journeyStep.getDeparture()));
                                char[] bitmap = ((String) recordFields.get("bitmap")).toCharArray();

                                if(bitmap.length > daysSinceTimetableChange) {
                                    char bitmapDay = bitmap[daysSinceTimetableChange];

                                    // when bitmap is X, then the train is in service
                                    if(bitmapDay == 'X') {
                                        // close response connection
                                        response.close();

                                        // set train string to block_bezeichnung, it's the train type
                                        journeyStep.setTrain(
                                                (String) recordFields.get("block_bezeichnung")
                                        );
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }

                // ensure response is closed.
                response.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return;
    }
}
