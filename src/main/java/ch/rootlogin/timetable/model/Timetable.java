package ch.rootlogin.timetable.model;

import ch.rootlogin.timetable.pojo.TimetablePojo;

import java.time.LocalDateTime;
import java.util.List;

public class Timetable {
    private String from;
    private String to;
    private LocalDateTime when = LocalDateTime.now();
    private TimetablePojo response;
    private List<Connection> connections;

    public Timetable(String from, String to) {
        this.from = from;
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public LocalDateTime getWhen() {
        return when;
    }

    public void setWhen(LocalDateTime when) {
        this.when = when;
    }

    public TimetablePojo getResponse() {
        return response;
    }

    public void setResponse(TimetablePojo response) {
        this.response = response;
    }

    public List<Connection> getConnections() {
        return connections;
    }

    public void setConnections(List<Connection> connections) {
        this.connections = connections;
    }
}
