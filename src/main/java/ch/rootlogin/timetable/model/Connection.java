package ch.rootlogin.timetable.model;

import java.time.LocalDateTime;
import java.util.List;

public class Connection {
    private LocalDateTime departure;
    private LocalDateTime arrival;
    private String platform;

    private List<JourneyStep> journeySteps;

    public Connection(LocalDateTime departure, LocalDateTime arrival, String platform) {
        this.departure = departure;
        this.arrival = arrival;
        this.platform = platform;
    }

    public LocalDateTime getDeparture() {
        return departure;
    }

    public void setDeparture(LocalDateTime departure) {
        this.departure = departure;
    }

    public LocalDateTime getArrival() {
        return arrival;
    }

    public void setArrival(LocalDateTime arrival) {
        this.arrival = arrival;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public List<JourneyStep> getJourneySteps() {
        return journeySteps;
    }

    public void setJourneySteps(List<JourneyStep> journeySteps) {
        this.journeySteps = journeySteps;
    }
}
