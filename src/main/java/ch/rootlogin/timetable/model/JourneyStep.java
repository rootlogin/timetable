package ch.rootlogin.timetable.model;

import java.time.LocalDateTime;

public class JourneyStep extends Connection {
    private String from;
    private String to;
    private String trainName;
    private String operator;
    private String product;
    private String train;

    public JourneyStep(String from, String to, LocalDateTime departure, LocalDateTime arrival, String platform) {
        super(departure, arrival, platform);

        this.from = from;
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainNumber) {
        this.trainName = trainNumber;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getTrain() {
        return train;
    }

    public void setTrain(String train) {
        this.train = train;
    }
}
